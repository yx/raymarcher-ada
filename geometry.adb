with Vectors; use Vectors;

package body Geometry is
	-- get distance to sphere
	function SDF (V : Vector_3; S : Sphere) return Float is
	begin
		return Length (V - S.Position) - S.Radius;
	end SDF;
end Geometry;
