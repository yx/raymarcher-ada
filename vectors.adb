with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Vectors is
	-- normalize
	procedure Normalize (V : in out Vector_3) is 
		L : Float := Length (V);
	begin
		V.X := V.X / L;
		V.Y := V.Y / L;
		V.Z := V.Z / L;
	end Normalize;
	
	-- vector length
	function Length (V : Vector_3) return Float is
	begin
		return Sqrt (V.X ** 2 + V.Y ** 2 + V.Z ** 2);
	end Length;

	-- vector addition
	function "+" (Left, Right : Vector_3) return Vector_3 is
		Result : Vector_3 := Vector_3'(Left.X + Right.X, Left.Y + Right.Y, Left.Z + Right.Z);
	begin
		return Result;
	end "+";

	-- vector subtraction
	function "-" (Left, Right : Vector_3) return Vector_3 is
		Result : Vector_3 := Vector_3'(Left.X - Right.X, Left.Y - Right.Y, Left.Z - Right.Z);
	begin
		return Result;
	end "-";

	-- vector scaling
	function "*" (Left : Vector_3; Right : Float) return Vector_3 is
		Result : Vector_3 := Vector_3'(Left.X * Right, Left.Y * Right, Left.Z * Right);
	begin
		return Result;
	end "*";
end Vectors;
