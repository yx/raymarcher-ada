package Vectors is
	type Vector_3 is record
		X, Y, Z : Float;
	end record;

	procedure Normalize (V : in out Vector_3);
	
	function Length (V : Vector_3) return Float;

	-- operators
	function "+" (Left, Right : Vector_3) return Vector_3;
	function "-" (Left, Right : Vector_3) return Vector_3;
	function "*" (Left : Vector_3; Right : Float) return Vector_3;
end Vectors;
