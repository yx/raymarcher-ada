with Vectors; use Vectors;

package Geometry is 
	type Sphere is record
		Position : Vector_3;
		Radius   : Float;
	end record;

	function SDF (V : Vector_3; S : Sphere) return Float;
end Geometry;
