with Ada.Text_IO; use Ada.Text_IO;
with Curses;
with Interfaces.C; use Interfaces.C;
with Vectors; use Vectors;

with Interfaces.C; use Interfaces.C; -- needed to get values from SDL functions

package body Raymarcher is
	procedure Init (Cam : Vector_3) is
	begin
		Scene := Sphere'(Vector_3'(0.0, 0.0, 0.0), 1.0);
		Camera := Cam;
	end Init;

	procedure March_Scene is
		Current_Dir, Plane_X, Plane_Y : Vector_3; -- picture vectors
		Depth : Float;
		C : char; -- what character we're going to print to stdout
	begin
		for Y in 0 .. Win_Height - 1 loop
			Plane_Y := View_Plane_V * ((Float(Y) * View_Height / Float(Win_Height)) - View_Height / 2.0);
			for X in 0 .. Win_Width - 1 loop
				Plane_X := View_Plane_H * ((Float(X) * View_Width / Float(Win_Width)) - View_Width / 2.0);
				Current_Dir := Plane_X + Plane_Y + Direction;
				Normalize(Current_Dir);
				Depth := March_Ray(Current_Dir);
				C := Chars(Integer(Float(Chars'Last) * Depth / Depth_End));
				Curses.Move_Add_Character(int(Y), int(X), C);
			end loop;
		end loop;
	end March_Scene;

	function March_Ray (Dir : Vector_3) return Float is
		Depth, Dist : Float;
	begin
		Depth := Depth_Start;
		for I in 0 .. Max_Steps loop
			Dist := SDF (Camera + Dir * Depth, Scene);
			if Dist < Epsilon then
				return Depth;
			end if;
			Depth := Depth + Dist;
			exit when Depth >= Depth_End;
		end loop;
		return Depth_End;
	end March_Ray;
end Raymarcher;
