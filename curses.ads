with Interfaces.C; use Interfaces.C;

package Curses is
	type Cursor_Visibility is (Invisible, Visible, Very_Visible)
	   with
	     Convention   => C;
	procedure Cursor_Set (Visibility: Cursor_Visibility)
	  with
	    Import        => True,
	    Convention    => C,
	    External_Name => "curs_set";
	procedure Init
	  with
	    Import        => True,
	    Convention    => C,
	    External_Name => "initscr";
	procedure Move_Add_Character (Row, Col : int; C : char)
	  with
	    Import        => True,
	    Convention    => C,
	    External_Name => "mvaddch";
	procedure No_Echo
	  with
	    Import        => True,
	    Convention    => C,
	    External_Name => "noecho";
	procedure Quit
	  with
	    Import        => True,
	    Convention    => C,
	    External_Name => "endwin";
	procedure Raw
	  with
	    Import        => True,
	    Convention    => C,
	    External_Name => "raw";
	procedure Refresh
	  with
	    Import        => True,
	    Convention    => C,
	    External_Name => "refresh";

	function Get_Character return char 
	  with
	    Import        => True,
	    Convention    => C,
	    External_Name => "getch";
end Curses;
