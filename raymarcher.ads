with Ada.Numerics; use Ada.Numerics;
with Geometry; use Geometry;
with Interfaces.C; use Interfaces.C;
with Vectors; use Vectors;

package Raymarcher is 
	-- minimum and maximum march distances
	Depth_Start  : constant Float := 0.01;
	Depth_End    : constant Float := 10.0;
	Max_Steps    : constant Integer := 200;
	Epsilon      : constant Float := 0.01;
	-- camera stuff
	Camera       : Vector_3; -- camera position
	Direction    : Vector_3 := Vector_3'(1.0, 0.0, 0.0);
	View_Width   : constant Float := 1.0;
	View_Height  : constant Float := 1.0;
	View_Plane_H : Vector_3 := Vector_3'(0.0, 0.0, View_Width);
	View_Plane_V : Vector_3 := Vector_3'(0.0, View_Height, 0.0); -- view planes
	-- scene stuff
	Scene        : Sphere;
	-- window stuff
	Win_Width    : constant Integer := 40; -- width
	Win_Height   : constant Integer := 30; -- height
	-- anything here will work long as there's a space at the end
	Chars        : constant array (Positive range <>) of char := "#@O$%*o+-. ";

	procedure Init (Cam : Vector_3);
	procedure March_Scene;
	
	-- march ray through scene and return depth
	function March_Ray(Dir : Vector_3) return Float;
end Raymarcher;
