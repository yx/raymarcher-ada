with Ada.Text_IO; use Ada.Text_IO;
with Curses;
with Interfaces.C; use Interfaces.C;
with Raymarcher;
with Vectors; use Vectors;

procedure Main is
	Cam : Vector_3;
	Key : char;
begin
	Cam := Vector_3'(-3.0, 0.0, 0.0);
	-- ncurses time
	Curses.Init;
	Curses.No_Echo;
	Curses.Cursor_Set (Curses.Invisible);
	-- initialize scene and camera
	Raymarcher.Init (Cam);

	loop
		Raymarcher.March_Scene;
		Curses.Refresh;
		Key := Curses.Get_Character;
		case Key is
			when 'q' => exit;
			when others => null;
		end case;
	end loop;

	Curses.Quit;
end Main;
